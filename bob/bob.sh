#!/usr/bin/env bash

function answer() {
    echo $1
    exit
}

function all_caps() {

    if [[ ! $str =~ [a-z] && $str =~ [A-Z] ]]; then
        return 0
    fi    
    return 1    
}

str="${1//[[:space:]]/}"
len=${#str}

if [[ -z "$str" ]]; then
    answer "Fine. Be that way!"
fi

if [[ ${str:(-1):1} == '?' ]]; then

    all_caps && answer "Calm down, I know what I'm doing!"  
    
    answer "Sure."
fi    

all_caps && answer "Whoa, chill out!"

answer "Whatever."
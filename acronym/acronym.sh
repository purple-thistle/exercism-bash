#!/usr/bin/env bash

read -ra words <<< $1

for i in "${words[@]}"; do

    if [[ $i =~ ^[a-zA-Z] ]]; then
        acronym+=${i:0:1}
        
        if [[ $i == *"-"* ]]; then  
            for (( j=1; j < ${#i}; j++ )); do
                if [[ ${i:j:1} == "-" ]]; then
                    (( k=j+1 ))
                    acronym+=${i:k:1} 
                    break
                fi
            done        
        fi
    
    elif [[ ${i:0:1} == "_" ]]; then
        
        for (( j=1; j < ${#i}; j++ )); do
            if [[ ${i:j:1} != "_" ]]; then
                acronym+=${i:j:1} 
                break
            fi    
        done
    fi
done

echo "${acronym^^}"
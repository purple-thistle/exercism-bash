#!/usr/bin/env bash

num=$1
power=${#1}

for ((i=0; i<power; i++)); do
    (( sum+=(( ${num:i:1} ** power )) ))
done

(( sum == num )) && echo true && exit
echo false
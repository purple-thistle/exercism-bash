#!/usr/bin/env bash

declare -A proteins

function assign(){
    val=$1
    shift
    for i in $@; do
        proteins[$i]="$val"
    done
}

assign Methionine AUG
assign Phenylalanine UUU UUC
assign Leucine UUA UUG
assign Serine UCU UCC UCA UCG
assign Tyrosine UAU UAC
assign Cysteine UGU UGC
assign Tryptophan UGG
assign STOP UAA UAG UGA

declare -a res

len=${#1}

for((i=0;i<len;i+=3)); do
    char=${1:i:3}
    
    [[ ! -v proteins[$char] ]] && echo "Invalid codon" && exit 1
    
    [[ ${proteins[$char]} == "STOP" ]] && break

    res+=("${proteins[$char]}")
done

echo "${res[@]}"   
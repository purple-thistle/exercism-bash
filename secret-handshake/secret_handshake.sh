#!/usr/bin/env bash

bin=$(echo "obase=2; $1" | bc)

reverse=false

(( bin%10 != 0 )) && handshake+=('wink') && (( bin-- ))

(( bin%100 != 0 )) && handshake+=('double blink') && (( bin-=10 ))

(( bin%1000 != 0 )) && handshake+=('close your eyes') && (( bin-=100 ))

(( bin%10000 != 0 )) && handshake+=('jump') && (( bin-=1000 ))

(( bin%100000 != 0 )) && reverse=true


if $reverse; then
    for ((i = ${#handshake[@]} - 1; i >= 0; i--)); do
    printf '%s%s' "$sep" "${handshake[i]}"
    sep=","
done


else    
    IFS=,
    echo "${handshake[*]}"
fi    


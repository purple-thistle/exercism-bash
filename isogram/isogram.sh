#!/usr/bin/env bash

word=${1,,}
len=${#word}

for((i=0;i<len;i++)); do
    char1=${word:i:1}
    for((j=i+1;j<len;j++)); do
        char2=${word:j:1}
        [[ $char2 == [[:alpha:]]  && $char2 == $char1 ]] && echo false && exit
    done
done

echo true
#!/usr/bin/env bash

str=${1,,}

for i in {a..z}; do
    if [[ $str != *"$i"* ]]; then
        echo false
        exit
    fi    
done

echo true
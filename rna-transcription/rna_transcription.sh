#!/usr/bin/env bash

declare -A subs

subs[A]="U"
subs[T]="A"
subs[C]="G"
subs[G]="C"

len=${#1}

for((i=0;i<len;i++)); do
    char=${1:i:1}
    
    [[ ! -v subs[$char] ]] && echo "Invalid nucleotide detected." && exit 1
    
    res+=${subs[$char]}
done

echo "$res"
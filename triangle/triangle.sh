#!/usr/bin/env bash


function answer() {
    echo $1
    exit
}

function equilateral() {
    if [[ $1 == $2 && $2 == $3 ]]; then
        answer true
    fi
    answer false
}

function isosceles() {
    if [[ $1 == $2 || $1 == $3 || $2 == $3  ]]; then 
        answer true
    fi
    answer false
}

function scalene() {
    if [[ $1 != $2 && $1 != $3 && $2 != $3 ]]; then
        answer true
    fi
    answer false
}

function inequality() {
   
    if [[ $1 == 0 && $2 == 0 && $3 == 0 ]]; then
        answer false
    fi    
    
    (( $(echo "($1 + $2) < $3 ||  ($1 + $3) < $2 || ($3 + $2) < $1" |bc -l) )) && answer false
}

subcommand=$1
shift
case $subcommand in
    equilateral | isosceles | scalene) inequality "$@"
                                       $subcommand "$@" ;;
    *) echo "Unknow command"
        exit 1;;
esac    


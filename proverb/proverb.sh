#!/usr/bin/env bash

length=$#

if [[ length -ne 0 ]]; then

    for (( i=1; i < $length; i++ )); do
        j=$((i+1))
        printf "For want of a %s the %s was lost.\n" "${!i}" "${!j}"  
        
    done    

    printf "And all for the want of a %s." "$1"

fi  

echo ""



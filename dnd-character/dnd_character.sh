#!/usr/bin/env bash

function dice_sum(){
    local min=6
    for i in {1..4}; do
        num=$(shuf -i 1-6 -n 1)
        (( num < min )) && min=num
        (( sum+=num ))
    done
    (( sum-=min ))
    echo $sum
}

function modifier(){
    local constitution=$1
    (( mod=(constitution - 10)/2 ))
    (( constitution <= 9 )) && (( (constitution - 10) % 2 != 0 )) && (( mod-- ))
    echo $mod
}

function generate(){
    declare -A characteristics

    for i in "strength" "dexterity" "constitution" "intelligence" "wisdom" "charisma"; do
        characteristics[$i]="$(dice_sum)"
    done

    local modifier=$(modifier ${characteristics[constitution]}) 
    (( points=10 + modifier ))
    characteristics[hitpoints]="$points"

    for key in "${!characteristics[@]}" ; do
        printf "%s %s\n" "$key" "${characteristics[$key]}"
    done
}


subcommand=$1
shift
case $subcommand in
    generate | modifier) $subcommand "$@" ;;
    *) echo "Unknown subcommand" ;;
esac


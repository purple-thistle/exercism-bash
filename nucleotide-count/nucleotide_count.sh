#!/usr/bin/env bash

declare -A nucleotides=([A]=0 [C]=0 [G]=0 [T]=0)

len=${#1}

for(( i=0; i<len; i++ )); do
    char=${1:i:1}
    [[ ! -v nucleotides[$char] ]] && echo "Invalid nucleotide in strand" && exit 1
    ((nucleotides[$char]++))
done

for key in "A" "C" "G" "T" ; do
    printf "%s: %s\n" "$key" "${nucleotides[$key]}"
done
#!/usr/bin/env bash

declare -A letters

function assign(){
    val=$1
    shift
    for i in $@; do
        letters[$i]="$val"
    done
}

assign 1 a e i o u l n r s t
assign 2 d g
assign 3 b c m p
assign 4 f h v w y
assign 5 k
assign 8 j x
assign 10 q z

word=${1,,}
len=${#word}
score=0

for((i=0;i<len;i++)); do
    ((score+=letters[${word:i:1}]))
done

echo $score
#!/usr/bin/env bash

declare -A encoder
declare -A decoder

alphabet=" abcdefghijklmnopqrstuvwxyz" #space in the beginning for 1-indexed string
len=${#alphabet}


for ((i=1;i<len;i++)); do
    encoder[${alphabet:i:1}]="${alphabet:(-i):1}"
    decoder[${alphabet:(-i):1}]="${alphabet:i:1}"
done

#declare -p encoder decoder

function code(){
    word="${1//[[:space:]]/}"
    word=${word,,}
    length=${#word}
    command=$2
   
    for((i=0; i<length; i++)); do
        char=${word:i:1}
        
        if [[ -v encoder[$char] ]]; then
            [[ $command == "encode" ]] && char="${encoder[$char]}" || char="${decoder[$char]}"
        fi
        
        [[ $char =~ [[:alnum:]] ]] && res+="$char"
    done   

    echo "$res"
}


function print_five(){
    res=$1
    len=${#res}
    for((i=0; i<len; i++)); do
        (( i%5 == 0)) && printf "%s" "$sep"
        printf "%s" "${res:i:1}"
        sep=" "
    done
    echo
}


case $1 in 
    encode) print_five "$(code "$2" "$1")";;
    decode) code "$2" "$2";;
    *) echo "Uknown command"
       exit 1;;
esac
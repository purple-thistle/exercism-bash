#!/usr/bin/env bash

#(( $1 < 1 || $1 > 64 )) && echo "Error: invalid input" && exit 1
shopt -s extglob

function grains() {
    (( $1 < 1 || $1 > 64 )) && echo "Error: invalid input" && exit 1
    (( power = $1 - 1 ))
    echo "2^$power" | bc -l    
}

function total(){
    local res="0"
    for((i=1;i<=64;i++)); do
        res+="+ $(grains $i) "
    done
    echo "$res"| bc -l
}


case $1 in 
    +([[:digit:]]) )grains $1;;

    total) total;;

    *)echo "Error: invalid input"
      exit 1;;
esac    


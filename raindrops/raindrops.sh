#!/usr/bin/env bash


if [[ $1%3 -eq 0 ]]; then
    result+="Pling"
fi    
if [[ $1%5 -eq 0 ]]; then
    result+="Plang"  
fi
if [[ $1%7 -eq 0 ]]; then
    result+="Plong"  
fi

echo ${result:-$1}